//
//  AppIconController.m
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4MessageView.h"
#import "SK4BoolStringTransformer.h"

#import "AppIconAdmin.h"
#import "AppIconController.h"

@interface AppIconController ()
{
	AppIconAdmin* _admin;
}
@end

@implementation AppIconController

- (id)initWithFlag:(BOOL)ios
{
	self = [super init];
	if( self == nil )
		return nil;

	//		初期化

	_ios = ios;

	[[NSBundle mainBundle] loadNibNamed:@"BasicConvertWindow" owner:self topLevelObjects:nil];

	return self;
}

- (void)awakeFromNib
{
	//		初期化

	NSWindow* win = self.window;
	SK4MessageView* mv = self.messageView;

	if( self.ios ) {
		self.settingsKey = @"AppIcon_iOS";
		win.title = @"Create App Icon for iOS";
		mv.message = @"Please drop square image file.\n(Recommended 1024x1024 24bpp)";
	} else {
		self.settingsKey = @"AppIcon_OSX";
		win.title = @"Create App Icon for OS X";
		mv.message = @"Please drop square image file.\n(Recommended 1024x1024 32bpp)";

		[self.typeLabel setHidden:YES];
		[self.typeCombo setHidden:YES];
	}

	[win setFrameAutosaveName:[self makeKey:@"AppIconWindow"]];

	[super awakeFromNib];
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row
{
	AppIconUnit* unit = [[self.arrayController arrangedObjects] objectAtIndex:row];
	NSImage* img = unit.convertImage;

	NSImageView* iv = [self drawerImageView];
	if( unit.hasAlpha == NO ) {
		iv.image = img;
	} else {
		iv.image = sk4TransparentBackground( img );
	}

	return YES;
}

////////////////////////////////////////////////////////////////

- (void)setupTableView
{
	//		テーブルビューを設定

	NSTableView* tv = self.tableView;
	NSArrayController* ac = self.arrayController;
	NSTableColumn* col = nil;
	NSDictionary* opt = @{ NSValueTransformerNameBindingOption:SK4_BOOL_STRING_TRANSFORMER };

	col = [tv tableColumnWithIdentifier:@"flagColumn"];
	[col bind:@"value" toObject:ac withKeyPath:@"arrangedObjects.convertFlag" options:nil];

	col = [self makeTableColumn:@"Filename" width:220 align:NSLeftTextAlignment editable:YES];
	[col bind:@"value" toObject:ac withKeyPath:@"arrangedObjects.filename" options:nil];
	[tv addTableColumn:col];

	[self addTableColumn:@"Use" width:80 path:@"useString" options:nil];
	[self addTableColumn:@"Type" width:80 path:@"typeString" options:nil];

	[self addTableColumn:@"Require" width:48 path:@"require" options:opt];
	[self addTableColumn:@"Retina" width:48 path:@"retina" options:opt];

	col = [self makeTableColumn:@"Size" width:80 align:NSCenterTextAlignment editable:NO];
	[col bind:@"value" toObject:ac withKeyPath:@"arrangedObjects.sizeString" options:nil];
	[tv addTableColumn:col];
}

- (void)convert
{
	//		変換処理を実行

	[_admin convert:self.outputDir.stringValue];
}

////////////////////////////////////////////////////////////////

- (void)setupItemArray
{
	//		アイテムの一覧を初期化

	_admin = [[AppIconAdmin alloc] init];

	if( _ios )
		_admin.admin = [self.typeCombo indexOfSelectedItem];
	else
		_admin.admin = APP_ICON_ADMIN_OSX;
}

- (void)addItemArray:(NSString*)file path:(NSString*)path
{
	//		アイテムの一覧にファイルを追加

	[_admin addFile:file path:path];
}

- (NSArray*)itemArray
{
	//		アイテムの一覧を返す

	return _admin.unitArray;
}

@end
