//
//  BarCutUnit.m
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4BitmapContext.h"

#import "BarCutUnit.h"
#import "BarCutInfo.h"
#import "AppDelegate.h"

@implementation BarCutUnit

- (BOOL)readFile:(NSString*)file path:(NSString*)path
{
//	_path = path;

	_filename = file;
	_convertFlag = TRUE;

	NSString* full = [path stringByAppendingPathComponent:file];
	_image = [[NSImage alloc] initWithContentsOfFile:full];
	if( _image == nil )
		return FALSE;

	_size = sk4ImagePixelSize( _image );
	_info = [appDelegate().barCutInfo infoString:_size];
	if( _info == nil ) {
		_info = @"No match.";
		_convertFlag = FALSE;
	}

	return TRUE;
}

- (BOOL)execFile:(NSString*)outdir
{
	//		転送先のイメージを作成

	NSInteger cut = [appDelegate().barCutInfo cutSize:_size];
	CGRect src_re = CGRectMake( 0, cut, _size.width, _size.height - cut );

	SK4BitmapContext* con = [[SK4BitmapContext alloc] initWithSize:src_re.size alpha:NO];
	[con drawImage:_image toRect:CGRectZero fromRect:src_re];

	NSData* data = [con makePngData];
	NSString* output = [outdir stringByAppendingPathComponent:_filename];
	[data writeToFile:output atomically:YES];

	_convertFlag = FALSE;

	return YES;
}

////////////////////////////////////////////////////////////////

- (NSString*)sizeString
{
	return sk4Printf( @"%gx%g", _size.width, _size.height );
}

@end
