//
//  AppIconAdmin.m
//  Minerba
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"

#import "AppIconAdmin.h"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//		アイコンに関する情報

//		TODO: plistから読み込むようにする

/*
 参考資料（2014/03/29）

 iOSヒューマンインターフェイスガイドライン: アイコンや画像の大きさ
 https://developer.apple.com/jp/devcenter/ios/library/documentation/userexperience/conceptual/mobilehig/IconMatrix/IconMatrix.html#//apple_ref/doc/uid/TP40006556-CH27-SW1
 */
struct {
	NSInteger use;
	NSInteger type;

	BOOL require;
	BOOL retina;
	CGFloat width;
	CGFloat height;

} g_typeInfo[] = {

	//		for iOS

	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_IPHONE,	YES, YES,  120,  120 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_IPAD,		YES, YES,  152,  152 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_IPAD,		YES,  NO,   76,   76 },

	{ APP_ICON_USE_SPOTLIGHT,	APP_ICON_TYPE_IPHONE,	 NO, YES,   80,   80 },
	{ APP_ICON_USE_SPOTLIGHT,	APP_ICON_TYPE_IPAD,		 NO, YES,   80,   80 },
	{ APP_ICON_USE_SPOTLIGHT,	APP_ICON_TYPE_IPAD,		 NO,  NO,   40,   40 },

	{ APP_ICON_USE_SETTINGS,	APP_ICON_TYPE_IPHONE,	 NO, YES,   58,   58 },
	{ APP_ICON_USE_SETTINGS,	APP_ICON_TYPE_IPAD,		 NO, YES,   58,   58 },
	{ APP_ICON_USE_SETTINGS,	APP_ICON_TYPE_IPAD,		 NO,  NO,   29,   29 },

	{ APP_ICON_USE_APP_STORE,	APP_ICON_TYPE_IPHONE,	YES, YES, 1024, 1024 },
	{ APP_ICON_USE_APP_STORE,	APP_ICON_TYPE_IPAD,		YES, YES, 1024, 1024 },

	//		for OS X

	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_OSX,		YES, YES,   16,   16 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_OSX,		YES, YES,   32,   32 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_OSX,		YES, YES,   64,   64 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_OSX,		YES, YES,  128,  128 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_OSX,		YES, YES,  256,  256 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_OSX,		YES, YES,  512,  512 },
	{ APP_ICON_USE_APP_ICON,	APP_ICON_TYPE_OSX,		YES, YES, 1024, 1024 },

};

int g_typeInfoMax = sizeof(g_typeInfo)/sizeof(g_typeInfo[0]);


////////////////////////////////////////////////////////////////

@implementation AppIconAdmin

- (id)init
{
	self = [super init];
	if( self == nil )
		return nil;
	
	//		初期化

	_unitArray = [[NSMutableArray alloc] init];

	return self;
}


- (void)addFile:(NSString*)file path:(NSString*)path
{
	NSString* full = [path stringByAppendingPathComponent:file];

	NSImage* img = [[NSImage alloc] initWithContentsOfFile:full];
	NSString* fn = [[full lastPathComponent] stringByDeletingPathExtension];

	[self makeConvertArray:img filename:fn];
}

- (void)makeConvertArray:(NSImage*)image filename:(NSString*)fn
{
	for( NSInteger use=0; use<APP_ICON_USE_MAX; use++ ) {
		switch( _admin ) {
			case APP_ICON_ADMIN_IOS:
				[self makeConvertOne:APP_ICON_TYPE_IPHONE use:use image:image filename:fn];
				[self makeConvertOne:APP_ICON_TYPE_IPAD use:use image:image filename:fn];
				break;

			case APP_ICON_ADMIN_IPHONE:
				[self makeConvertOne:APP_ICON_TYPE_IPHONE use:use image:image filename:fn];
				break;

			case APP_ICON_ADMIN_IPAD:
				[self makeConvertOne:APP_ICON_TYPE_IPAD use:use image:image filename:fn];
				break;

			case APP_ICON_ADMIN_OSX:
				[self makeConvertOne:APP_ICON_TYPE_OSX use:use image:image filename:fn];
				break;

			default:
				break;
		}
	}
}

- (void)makeConvertOne:(NSInteger)type use:(NSInteger)use image:(NSImage*)image filename:(NSString*)fn
{
	for( int i=0; i<g_typeInfoMax; i++ ) {
		if( g_typeInfo[i].type != type || g_typeInfo[i].use != use )
			continue;

		AppIconUnit* icon = [[AppIconUnit alloc] init];
		CGSize size;
		size.width = g_typeInfo[i].width;
		size.height = g_typeInfo[i].height;

		[icon setupUse:use type:type require:g_typeInfo[i].require retina:g_typeInfo[i].retina size:size];
		[icon setupImage:image filename:fn];

		[_unitArray addObject:icon];
	}
}

////////////////////////////////////////////////////////////////

- (void)convert:(NSString*)output
{
	NSMutableArray* ar = [[NSMutableArray alloc] init];

	for( AppIconUnit* icon in _unitArray ) {

		if( icon.convertFlag == NO )
			continue;

		if( sk4SearchString( ar, icon.filename ) >= 0 ) {
			icon.convertFlag = NO;
			continue;
		}

		[icon createIcon:output];

		[ar addObject:icon.filename];
	}
}

@end

// eof
