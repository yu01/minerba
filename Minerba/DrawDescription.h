//
//  DrawDescription.h
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DrawDescription : NSObject

@property (nonatomic) CGRect frame;
@property (nonatomic) NSString* title;
@property (nonatomic) NSColor* titleColor;

@property (nonatomic) NSString* detail;
@property (nonatomic) NSColor* detailColor;

- (id)initWithFrame:(NSRect)frame;
- (void)makeTextAtr;
- (void)draw:(CGContextRef)cg;

@end
