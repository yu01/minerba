//
//  AppIconController.h
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "BasicConvertController.h"

@interface AppIconController : BasicConvertController

@property (readonly, nonatomic) BOOL ios;

- (id)initWithFlag:(BOOL)ios;

@end
