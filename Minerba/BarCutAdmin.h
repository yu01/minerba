//
//  BarCutAdmin.h
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BarCutUnit.h"

@interface BarCutAdmin : NSObject

@property (readonly, nonatomic) NSMutableArray* unitArray;

- (void)addFile:(NSString*)file path:(NSString*)path;
- (int)convert:(NSString*)outdir;

@end
