//
//  BarCutAdmin.m
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "BarCutAdmin.h"

@implementation BarCutAdmin

- (id)init
{
	self = [super init];
	if( self == nil )
		return nil;

	//		初期化

	_unitArray = [[NSMutableArray alloc] init];

	return self;
}

- (void)addFile:(NSString*)file path:(NSString*)path
{
	BarCutUnit* info = [[BarCutUnit alloc] init];

	if( [info readFile:file path:path] == FALSE )
		return;

	[_unitArray addObject:info];
}

- (int)convert:(NSString*)outdir
{
	int count = 0;

	for( BarCutUnit* fi in _unitArray ) {
		if( fi.convertFlag == NO )
			continue;

		if( [fi execFile:outdir] )
			count++;
	}

	return count;
}

@end
