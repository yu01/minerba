//
//  AppDelegate.m
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "SK4BoolStringTransformer.h"

#import "BarCutInfo.h"
#import "AppDelegate.h"

@implementation AppDelegate

+ (void)initialize
{
	[SK4BoolStringTransformer registTransformer];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Insert code here to initialize your application
}

////////////////////////////////////////////////////////////////

- (BarCutInfo*)barCutInfo
{
	if( _barCutInfo == nil )
		_barCutInfo = [[BarCutInfo alloc] init];

	return _barCutInfo;
}


@end
