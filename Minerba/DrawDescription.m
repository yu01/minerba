//
//  DrawDescription.m
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "DrawDescription.h"

@interface DrawDescription ()
{
	NSFont* _titleFont;

	NSDictionary* _titleAtr;
	NSDictionary* _detailAtr;
}
@end

@implementation DrawDescription

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(NSRect)frame
{
	self = [super init];
	if( self == nil )
		return nil;

	self.frame = frame;

	[self makeTextAtr];

    return self;
}

- (void)makeTextAtr
{
	NSInteger wx = _frame.size.height/2;

	NSFont* font = nil;
	NSColor* col = nil;

	NSMutableParagraphStyle* para = [[NSMutableParagraphStyle alloc] init];
	para.alignment = NSCenterTextAlignment;

	//		タイトル用

	font = [NSFont systemFontOfSize:wx-2];
	col = _titleColor;
	if( col == nil )
		col = [NSColor blueColor];

	_titleAtr = @{ NSFontAttributeName:font,
				   NSForegroundColorAttributeName:col,
				   NSParagraphStyleAttributeName:para };

	//		詳細用

	font = [NSFont systemFontOfSize:wx/2-4];
	col = _detailColor;
	if( col == nil )
		col = [NSColor blackColor];

	_detailAtr = @{ NSFontAttributeName:font,
					NSForegroundColorAttributeName:col,
					NSParagraphStyleAttributeName:para };
}

- (void)draw:(CGContextRef)cg
{
	//		まとめて描画

	CGRect rect = _frame;
	rect.origin.x = 0;
	rect.origin.y = 0;

	rect.size.height /= 2;
	[_detail drawInRect:rect withAttributes:_detailAtr];

	rect.origin.y = rect.size.height;
	[_title drawInRect:rect withAttributes:_titleAtr];

	NSColor* col = _titleColor;
	if( col == nil )
		col = [NSColor blueColor];

	[col setStroke];

	CGContextSetLineWidth( cg, 2 );
	CGContextMoveToPoint( cg, rect.origin.x, rect.origin.y );
	CGContextAddLineToPoint( cg, rect.origin.x + rect.size.width, rect.origin.y );
	CGContextStrokePath( cg );
}

@end
