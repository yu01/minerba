//
//  AppIconAdmin.h
//  Minerba
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppIconUnit.h"

enum {
	APP_ICON_ADMIN_IOS = 0,
	APP_ICON_ADMIN_IPHONE,
	APP_ICON_ADMIN_IPAD,
	APP_ICON_ADMIN_OSX,
};

@interface AppIconAdmin : NSObject

@property (nonatomic) NSInteger admin;		//	ex) APP_ICON_ADMIN_IOS
@property (nonatomic) NSMutableArray* unitArray;

- (void)addFile:(NSString*)file path:(NSString*)path;
- (void)convert:(NSString*)output;

@end
