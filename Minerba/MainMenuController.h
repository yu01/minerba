//
//  MainMenuController.h
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

@class DescriptionView;

@interface MainMenuController : NSObject

@property (weak) IBOutlet DescriptionView *desc;


////////////////////////////////////////////////////////////////
//		for iOS

@property (weak) IBOutlet NSButton *iosAppIconButton;
- (IBAction)makeIosAppIcon:(id)sender;

@property (weak) IBOutlet NSButton *statusBarButton;
- (IBAction)cutStatusBar:(id)sender;




////////////////////////////////////////////////////////////////
//		for OSX

@property (weak) IBOutlet NSButton *osxAppIconButton;
- (IBAction)makeOsxAppIcon:(id)sender;

@end

// eof
