//
//  AppIconController.m
//  Minerba
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4MessageView.h"
#import "SK4BoolStringTransformer.h"

#import "BasicConvertController.h"

enum {
	BCC_TAG_DRAWER_IMAGE = 1,

};

@implementation BasicConvertController

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

/*
- (id)init
{
	self = [super init];
	if( self == nil )
		return nil;

	[[NSBundle mainBundle] loadNibNamed:@"BasicConvertWindow" owner:self topLevelObjects:nil];

	return self;
}
*/

- (void)awakeFromNib
{
	//		初期化

	_dropView.dropDelegate = self;
	_dropView.allowedFileTypes = [NSImage imageFileTypes];

	[self setupTableView];

	_tableView.delegate = self;
	[_tableView setTarget:self];
	[_tableView setDoubleAction:@selector(doubleClickAction:)];

	[self initUserDefaults];
	[self readUserDefaults];

	if( _inputArray.count == 0 )
		[_messageView setHidden:NO];
	else
		[_dropView droppedFileParse:_inputArray];
}

- (void)resetInfo
{
	[self setDrawerOpen:NO];

	[self setupItemArray];
	[self makeItemArray];
}

- (void)showWindow
{
	[_window makeKeyAndOrderFront:nil];
}

////////////////////////////////////////////////////////////////

- (NSString*)makeKey:(NSString*)base
{
	return [_settingsKey stringByAppendingString:base];
}

- (void)addTableColumn:(NSString*)title width:(NSInteger)width path:(NSString*)path options:(NSDictionary*)opt
{
	NSString* keyPath = sk4Printf( @"arrangedObjects.%@", path );

	NSTableColumn* col = [self makeTableColumn:title width:width];
	[col bind:@"value" toObject:_arrayController withKeyPath:keyPath options:opt];
	[_tableView addTableColumn:col];
}

- (NSTableColumn*)makeTableColumn:(NSString*)title width:(NSInteger)width
{
	return [self makeTableColumn:title width:width align:NSLeftTextAlignment editable:NO];
}

- (NSTableColumn*)makeTableColumn:(NSString*)title width:(NSInteger)width align:(NSTextAlignment)align editable:(BOOL)editable
{
	NSTableColumn* col = [[NSTableColumn alloc] init];
	col.width = width;

	NSTableHeaderCell* head = col.headerCell;
	head.title = title;
	head.alignment = align;

	NSTextFieldCell* data = col.dataCell;
	data.alignment = align;
	[data setEditable:editable];

	return col;
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - アクション

- (IBAction)changeTypeCombo:(NSComboBox *)sender
{
	[self writeUserDefaults];

	if( _inputArray.count != 0 )
		[_dropView droppedFileParse:_inputArray];
}

- (IBAction)flipDrawer:(id)sender
{
	if( [self isDrawerOpen] )
		[self setDrawerOpen:NO];
	else
		[self setDrawerOpen:YES];
}

- (IBAction)selectOutputDir:(id)sender
{
	NSOpenPanel* sp = [NSOpenPanel openPanel];
	sp.canChooseDirectories = YES;
	sp.canChooseFiles = NO;
	sp.allowsMultipleSelection = NO;
	sp.canCreateDirectories = YES;
	sp.title = @"Select Output Directory";

	NSInteger rc = [sp runModal];
	if( rc != NSOKButton )
		return;

	_outputDir.stringValue = [sp.URL path];

	[self writeUserDefaults];
}

- (IBAction)convertExec:(id)sender
{
	//		この時点で存在しないファイル・ディレクトリは無視

	NSFileManager* fm = [NSFileManager defaultManager];

	NSString* output = _outputDir.stringValue;

	BOOL dir_flag = NO;
	if( [fm fileExistsAtPath:output isDirectory:&dir_flag] == NO ) {
		NSString* mes = sk4Printf( @"Output directory does not exist.\n\n%@", output );
		sk4WarningAlert( mes );
		return;
	}

	if( dir_flag == NO ) {
		NSString* mes = sk4Printf( @"Output directory is not directory.\n\n%@", output );
		sk4WarningAlert( mes );
		return;
	}

	[self convert];
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - テーブルビュー関係

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row
{
	return YES;
}

- (void)doubleClickAction:(id)sender
{
	[self flipDrawer:nil];
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - ドロワー関係

- (BOOL)isDrawerOpen
{
	NSDrawerState st = _drawer.state;

	if( st == NSDrawerOpeningState || st == NSDrawerOpenState )
		return YES;
	else
		return NO;
}

- (void)setDrawerOpen:(BOOL)flag
{
	if( flag ) {
		[_drawer openOnEdge:NSMaxXEdge];
		_flipDrawerButton.title = @"<<";
	} else {
		[_drawer close];
		_flipDrawerButton.title = @">>";
	}
}

- (NSImageView*)drawerImageView
{
	return [_drawer.contentView viewWithTag:BCC_TAG_DRAWER_IMAGE];
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - ドラッグ＆ドロップ関係

- (void)dropFileParseStart:(NSArray*)files
{
	_inputArray = files;
	[self writeUserDefaults];

	[self setupItemArray];
}

- (void)dropFileParseFile:(NSString*)file path:(NSString*)path
{
	[self addItemArray:file path:path];
}

- (void)dropFileParseEnd
{
	[self makeItemArray];
}

- (void)makeItemArray
{
	//		アイテムの一覧を作成

	NSArray* ar = [self itemArray];

	self.arrayController.content = ar;

	if( ar.count == 0 ) {
		[self.messageView setHidden:NO];
		[[self drawerImageView] setImage:nil];

	} else {
		[self.messageView setHidden:YES];
		[self tableView:self.tableView shouldSelectRow:0];
	}
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - ユーザーデフォルト関係

SK4_DEFINE_STATIC( BCC_COMBO_TYPE );
SK4_DEFINE_STATIC( BCC_INPUT_DIR );
SK4_DEFINE_STATIC( BCC_OUTPUT_DIR );

- (void)initUserDefaults
{
	//		デフォルトを設定

	NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];

	NSString* key = [self makeKey:BCC_COMBO_TYPE];

	dic[key] = @(0);

//	NSString* key = [self makeKey:BCC_OUTPUT_DIR];
//	dic[key] = NSHomeDirectory();

	[self defaultSettings:dic];

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud registerDefaults:dic];
}

- (void)readUserDefaults
{
	//		ユーザーデフォルトを読み込み

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	NSString* key = nil;
	NSString* str = nil;
	NSInteger no = 0;

	//		コンボボックス

	key = [self makeKey:BCC_COMBO_TYPE];
	no = [ud integerForKey:key];
	if( no < 0 )
		no = 0;

	[self.typeCombo selectItemAtIndex:no];

	//		入力ファイル

	key = [self makeKey:BCC_INPUT_DIR];
	_inputArray = [ud arrayForKey:key];

	//		出力先

	key = [self makeKey:BCC_OUTPUT_DIR];
	str = [ud stringForKey:key];
	if( str == nil )
		str = @"";

	self.outputDir.stringValue = str;

	//		その他

	[self readSettings:ud];
}

- (void)writeUserDefaults
{
	//		ユーザーデフォルトを保存

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	NSString* key = nil;
	NSString* str = nil;
	NSInteger no = 0;

	//		コンボボックス

	key = [self makeKey:BCC_COMBO_TYPE];
	no = [self.typeCombo indexOfSelectedItem];
	if( no < 0 )
		no = 0;

	[ud setObject:@(no) forKey:key];

	//		入力ファイル

	key = [self makeKey:BCC_INPUT_DIR];
	[ud setObject:_inputArray forKey:key];

	//		出力先

	key = [self makeKey:BCC_OUTPUT_DIR];
	str = self.outputDir.stringValue;
	[ud setObject:str forKey:key];

	//		その他

	[self writeSettings:ud];

	[ud synchronize];
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - 仮想関数

- (void)setupTableView
{
	//		テーブルビューを設定
}

- (void)convert
{
	//		変換処理を実行
}

////////////////////////////////////////////////////////////////

- (void)setupItemArray
{
	//		アイテムの一覧を初期化
}

- (void)addItemArray:(NSString*)file path:(NSString*)path
{
	//		アイテムの一覧にファイルを追加
}

- (NSArray*)itemArray
{
	//		アイテムの一覧を返す

	return nil;
}

////////////////////////////////////////////////////////////////

- (void)defaultSettings:(NSMutableDictionary*)dic
{
}

- (void)readSettings:(NSUserDefaults*)ud
{
}

- (void)writeSettings:(NSUserDefaults*)ud
{
}

@end

// eof
