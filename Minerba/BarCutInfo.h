//
//  BarCutInfo.h
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BarCutInfo : NSObject

- (NSString*)infoString:(CGSize)size;
- (NSInteger)cutSize:(CGSize)size;

@end
