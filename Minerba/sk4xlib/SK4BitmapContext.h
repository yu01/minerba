//
//  SK4BitmapContext.h
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/20.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  ビットマップコンテキストを管理するクラス
 */
@interface SK4BitmapContext : NSObject


/**
 *  コンテキストのサイズ
 */
@property (nonatomic) CGSize size;


/**
 *  コンテキスト
 */
@property (readonly, nonatomic) CGContextRef context;


/**
 *  拡大・縮小の補間法
 */
@property (nonatomic) CGInterpolationQuality interpolationQuality;


/**
 *  ビットマップコンテキストの生成
 *
 *  @param size  サイズ
 *  @param alpha アルファ情報の有無
 *
 *  @return 自分自身
 */
- (id)initWithSize:(CGSize)size alpha:(BOOL)alpha;


////////////////////////////////////////////////////////////////
/// @name	イメージの描画

/**
 *  イメージを指定された位置に描画
 *
 *  @param image イメージ
 *  @param to    描画先の位置
 */
- (void)drawImage:(NSImage*)image toPos:(CGPoint)to;


/**
 *  イメージの指定された範囲を指定された位置に描画
 *
 *  @param image イメージ
 *  @param to    描画先の位置
 *  @param from  描画元の範囲
 */
- (void)drawImage:(NSImage*)image toPos:(CGPoint)to fromRect:(CGRect)from;


/**
 *  イメージの指定された範囲を指定された範囲に描画
 *
 *  @param image イメージ
 *  @param to    描画先の範囲
 *  @param from  描画元の範囲
 */
- (void)drawImage:(NSImage*)image toRect:(CGRect)to fromRect:(CGRect)from;


////////////////////////////////////////////////////////////////
/// @name	inline関係

/**
 *  ビットマップコンテキストからNSImageを生成
 *
 *  @return NSImage
 */
- (NSImage*)makeImage;


/**
 *  ビットマップコンテキストからNSBitmapImageRepを生成
 *
 *  @return NSBitmapImageRep
 */
- (NSBitmapImageRep*)makeBitmapImageRep;


/**
 *  ビットマップコンテキストからPNG形式のNSDataを生成
 *
 *  @return NSData
 */
- (NSData*)makePngData;

@end



// eof
