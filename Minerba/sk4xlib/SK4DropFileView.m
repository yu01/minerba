//
//  SK4DropFileView.m
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/13.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "SK4DropFileView.h"

@implementation SK4DropFileView

- (id)initWithFrame:(NSRect)frame
{
	self = [super initWithFrame:frame];
	if( self == nil )
		return nil;
	
	_selectHiddenFiles = YES;
	
	[self registerForDraggedTypes:@[NSFilenamesPboardType]];

	return self;
}

////////////////////////////////////////////////////////////////

- (void)awakeFromNib
{
	_selectHiddenFiles = YES;
	
	[self registerForDraggedTypes:@[NSFilenamesPboardType]];
}

-(BOOL)prepareForDragOperation:(id<NSDraggingInfo>)sender
{
	return YES;
}

-(NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender
{
	return NSDragOperationCopy;
}

-(NSDragOperation)draggingUpdated:(id<NSDraggingInfo>)sender
{
	return NSDragOperationCopy;
}

-(BOOL)performDragOperation:(id<NSDraggingInfo>)sender
{
	return YES;
}

-(void)concludeDragOperation:(id<NSDraggingInfo>)sender
{
	NSArray* files = [self filelistInDraggingInfo:sender];
	
	if( [self.dropDelegate respondsToSelector:@selector(dropFileReceived:)] ) {

		//		単純にドロップされたファイルを渡して終了
		
		[self.dropDelegate dropFileReceived:files];

	} else {

		//		ドロップされたファイルの解析まで行う
		
		[self droppedFileParse:files];
	}
}

-(NSArray*)filelistInDraggingInfo:(id<NSDraggingInfo>)info
{
	NSPasteboard* board = [info draggingPasteboard];

	return [board propertyListForType:NSFilenamesPboardType];
}

////////////////////////////////////////////////////////////////

- (void)droppedFileParse:(NSArray*)files
{
	//		ドロップされたファイルを解析

	//		解析開始
	
	if( [self.dropDelegate respondsToSelector:@selector(dropFileParseStart:)] )
		[self.dropDelegate dropFileParseStart:files];
	
	//		要素を順番に取り出し
	
	NSFileManager* fm = [NSFileManager defaultManager];
	
	for( id obj in files ) {

		NSString* file = nil;
		
		if( [obj isKindOfClass:[NSURL class]] ) {
			file = [obj path];

		} else if( [obj isKindOfClass:[NSString class]] ) {
			file = obj;

		} else {
			continue;

		}
		
		//		この時点で存在しないファイル・ディレクトリは無視
		
		BOOL dir_flag = NO;
		if( [fm fileExistsAtPath:file isDirectory:&dir_flag] == NO )
			continue;
		
		if( dir_flag )
			[self parseDir:file];
		else
			[self parseFile:file];
	}

	//		解析終了
	
	if( [self.dropDelegate respondsToSelector:@selector(dropFileParseEnd)] )
		[self.dropDelegate dropFileParseEnd];
}

- (void)parseDir:(NSString*)path
{
	//		ディレクトリの場合
	
	//		ディレクトリの中のファイル一覧を作成する
	
	NSFileManager* fm = [NSFileManager defaultManager];
	NSArray* files = [fm subpathsAtPath:path];

	for( NSString* file in files ) {
		[self parseOne:file path:path];
	}
}

- (void)parseFile:(NSString*)full
{
	//		ファイルの場合
	
	NSString* file = [full lastPathComponent];
	NSString* path = [full stringByDeletingLastPathComponent];
	
	[self parseOne:file path:path];
}

- (void)parseOne:(NSString*)file path:(NSString*)path
{
	//		ファイルを発見
	
	//		対象外のファイルか？
	
	unichar ch = [file characterAtIndex:0];
	if( _selectHiddenFiles == YES && ch == 46 )
		return;

	//		ファイルタイプを確認
	
	if( [self checkFileType:file] == NO )
		return;
	
	//		発見したファイルを通知
	
	if( [self.dropDelegate respondsToSelector:@selector(dropFileParseFile:path:)] )
		[self.dropDelegate dropFileParseFile:file path:path];
}

- (BOOL)checkFileType:(NSString*)file
{
	//		ファイルタイプを確認

	NSString* ext = [file pathExtension];

	for( NSString* type in _allowedFileTypes ) {
		if( [type compare:ext options:NSCaseInsensitiveSearch] == NSOrderedSame )
			return YES;
	}
	
	return NO;
}

@end
