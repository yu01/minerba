//
//  SK4MessageView.m
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/19.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "SK4MessageView.h"

@implementation SK4MessageView

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];

	CGRect re = self.bounds;
	
	const CGFloat gray = 0.2;
	
	CGContextRef cgcr = [[NSGraphicsContext currentContext] graphicsPort];
	
	CGContextSetRGBFillColor( cgcr, gray, gray, gray, 0.8 );
	CGContextFillRect( cgcr, re );
	
	if( _message == nil )
		return;
	
	NSFont* text_font = [NSFont systemFontOfSize:15];
	NSColor* text_col = [NSColor whiteColor];
	NSMutableParagraphStyle* text_ps = [[NSMutableParagraphStyle alloc] init];
	text_ps.alignment = NSCenterTextAlignment;
	NSDictionary* text_atr = @{ NSFontAttributeName:text_font,
								NSForegroundColorAttributeName:text_col,
								NSParagraphStyleAttributeName:text_ps };
	
	CGSize si = [_message sizeWithAttributes:text_atr];
	re.origin.y = - re.size.height / 2 + si.height / 2;
	
	[_message drawInRect:re withAttributes:text_atr];
}

@end
