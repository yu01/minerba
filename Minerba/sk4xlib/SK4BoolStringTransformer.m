//
//  SK4BoolStringTransformer.m
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#define SK4_DEFINE_TO_REALIZE
#import "SK4BoolStringTransformer.h"

@implementation SK4BoolStringTransformer

+ (Class)transformedValueClass
{
	return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
	return YES;
}

+ (void)registTransformer
{
	static BOOL regsiter = NO;

	if( regsiter )
		return;

	SK4BoolStringTransformer* tra = [[SK4BoolStringTransformer alloc] init];
//	[NSValueTransformer setValueTransformer:tra forName:@"SK4BoolStringTransformer"];
	[NSValueTransformer setValueTransformer:tra forName:SK4_BOOL_STRING_TRANSFORMER];

	regsiter = YES;
}

////////////////////////////////////////////////////////////////

- (id)transformedValue:(id)value
{
	if( value == nil )
		return nil;

	BOOL flag = NO;
	if( [value respondsToSelector:@selector(boolValue)] )
		flag = [value boolValue];

	if( flag )
		return @"Yes";
	else
		return @"No";
}

- (id)reverseTransformedValue:(id)value
{
	if( value == nil )
		return nil;

	BOOL flag = NO;
	if( [value respondsToSelector:@selector(compare:options:)] ) {
		if( [value compare:@"Yes" options:NSCaseInsensitiveSearch] == NSOrderedSame )
			flag = YES;
	}

	return @(flag);
}

@end

// eof
