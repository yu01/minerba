//
//  SK4BitmapContext.m
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/20.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4BitmapContext.h"

@implementation SK4BitmapContext

- (id)initWithSize:(CGSize)size alpha:(BOOL)alpha
{
	self = [super init];
	if( self == nil )
		return nil;
	
	//		初期化

	_size = size;
	
	CGBitmapInfo info = kCGBitmapByteOrderDefault;
	if( alpha )
		info |= kCGImageAlphaPremultipliedFirst;
	else
		info |= kCGImageAlphaNoneSkipFirst;

	NSInteger wx = size.width;
	NSInteger wy = size.height;

	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	_context = CGBitmapContextCreate( NULL, wx, wy, 8, wx * 4, space, info );
	CGColorSpaceRelease( space );

/*	
	//		for gray

	CGBitmapInfo info = kCGBitmapByteOrderDefault;
	CGColorSpaceRef space = CGColorSpaceCreateDeviceGray();
	_context = CGBitmapContextCreate( NULL, size.width, size.height, 8, size.width, space, info );	
	CGColorSpaceRelease( space );
*/
	
	return self;
}

- (void)dealloc
{
	if( _context != NULL ) {
		CGContextRelease( _context );
		_context = NULL;
	}
}

////////////////////////////////////////////////////////////////

- (void)drawImage:(NSImage*)image toPos:(CGPoint)to
{
	//		イメージを指定された位置に描画

	CGRect to_re;
	to_re.origin = to;
	to_re.size = sk4ImagePixelSize( image );

	//		実際の描画処理

	CGImageRef src_all = [image CGImageForProposedRect:NULL context:nil hints:nil];
	CGContextDrawImage( _context, to_re, src_all );
}

- (void)drawImage:(NSImage*)image toPos:(CGPoint)to fromRect:(CGRect)from
{
	//		イメージの指定された範囲を指定された位置に描画
	
	CGRect to_re;
	to_re.origin = to;
	to_re.size = from.size;
	
	[self drawImage:image toRect:to_re fromRect:from];
}

- (void)drawImage:(NSImage*)image toRect:(CGRect)to fromRect:(CGRect)from
{
	//		イメージの指定された範囲を指定された範囲に描画

	//		描画先の範囲が指定されてない場合、全体に描画
	
	if( CGRectIsEmpty( to ) )
		to.size = _size;

	//		描画元の範囲が指定されてない場合、全体を描画
	
	if( CGRectIsEmpty( from ) )
		from.size = sk4ImagePixelSize( image );

	//		実際の描画処理

	CGImageRef src_all = [image CGImageForProposedRect:NULL context:nil hints:nil];

	CGImageRef src_ref = CGImageCreateWithImageInRect( src_all, from );
	CGContextDrawImage( _context, to, src_ref );
	CGImageRelease( src_ref );
}

////////////////////////////////////////////////////////////////

- (CGInterpolationQuality)interpolationQuality
{
	return CGContextGetInterpolationQuality( _context );
}

- (void)setInterpolationQuality:(CGInterpolationQuality)quality
{
	CGContextSetInterpolationQuality( _context, quality );
}

////////////////////////////////////////////////////////////////

- (NSImage*)makeImage
{
	//		NSImageを作成

	CGImageRef image = CGBitmapContextCreateImage( _context );
	NSImage* img = [[NSImage alloc] initWithCGImage:image size:_size];
	CGImageRelease( image );

	return img;
}

- (NSBitmapImageRep*)makeBitmapImageRep
{
	//		NSBitmapImageRepを作成
	
	CGImageRef image = CGBitmapContextCreateImage( _context );
	NSBitmapImageRep* rep = [[NSBitmapImageRep alloc] initWithCGImage:image];
	CGImageRelease( image );
	
	return rep;
}

- (NSData*)makePngData
{
	//		PNGのNSDataを作成

	NSBitmapImageRep* rep = [self makeBitmapImageRep];
	NSDictionary* dic = @{ NSImageInterlaced:@(FALSE) };
	
	return [rep representationUsingType:NSPNGFileType properties:dic];
}

@end


// eof
