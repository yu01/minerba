//
//  AppDelegate.h
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class BarCutInfo;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@property (nonatomic) BarCutInfo* barCutInfo;

@end

inline static
AppDelegate* appDelegate( void )
{
	return (AppDelegate*)[[NSApplication sharedApplication] delegate];
}

